﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Planets
{
    public class Extra_Terrestrial : Planet
    {

        public string StarSystem { get; set; }


        public Extra_Terrestrial(string name, Composition composition, double diameter, double distanceFromTheSun, double mass, string description, string starSystem) : base(name, composition, diameter, distanceFromTheSun, mass, description)
        {
            this.StarSystem = starSystem;
        }
    }
}
