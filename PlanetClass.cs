﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;

namespace Planets
{ 

    public enum Helper
    {
        Order_By_Mass,
        Order_By_Diameter,
        Order_By_Planet,
        Get_Planet,
        Create_PLanet,
        None 

    }
    public enum Composition
    {
        NotClassified,
        Jovian, //Gas
        Terrestrial // Solid 

    }
    public class Planet
    {

        public string Name { get; private set; }
        public Composition Composition { get; private set; }
        public double Diameter { get; private set; }
        public double DistanceFromTheSun { get; private set; }
        public double Mass { get; private set; }
        public string Description { get; set; }

        public Helper GetHelp { get; set; }

        public Planet(string name, Composition composition, double diameter, double distanceFromTheSun, double mass, string description)
        {
            this.Name = name;
            this.Composition = composition;
            this.Diameter = diameter;
            this.DistanceFromTheSun = distanceFromTheSun;
            this.Mass = mass;
            this.Description = description;
        }

        public virtual string ToString()
        {
           



            string Msg1 = $"{Name} is Widely Known as a {Composition} Plant.{Description}.\n It is a stagering distance of {DistanceFromTheSun} Million KM From the sun. \r The mass of this planet is estimated to be about{Mass}M⊕ And a diamter of about {Diameter} KM";

            string Msg2 = ($"Name: {Name} Composition: {Composition}\n Mass: {Mass} M⊕\n Diameter: {Diameter} KM \n Distance From The Sun: {DistanceFromTheSun} Million KM  \n Description: {Description}");

            // Add Another Format

            var MessageFormat = new Random();
            var MessageList = new List<string> { Msg1, Msg2 };

            int i = MessageFormat.Next(MessageList.Count);
            return (MessageList[i]);

        }
        public static List<Planet>ListOfPlanets(List<Planet> Planets)
        {

            List<Planet> planets = new List<Planet>(); // planets is the out parameter 

            foreach (Planet planet in Planets)
            {
                planets.Add(planet);
            }

            return planets;
        }

        public static Helper Helper(Helper helpChoice, List<Planet> ListOfPlanets) 
        {

            if (helpChoice == Planets.Helper.Order_By_Diameter)
            {
                Program.OrderByDiameter(ListOfPlanets);
                return Planets.Helper.None;
            }

            else if (helpChoice == Planets.Helper.Order_By_Mass)
            {
                Program.OrderByMass(ListOfPlanets);
                return Planets.Helper.None;
            }
                

            else if (helpChoice == Planets.Helper.Order_By_Planet)
            {
                Program.OrderByPlanet(ListOfPlanets);
                return Planets.Helper.None;
            }

            else  if (helpChoice == Planets.Helper.Get_Planet)
            {
                // Insert Method here
                return Planets.Helper.None;
            }

            else if (helpChoice == Planets.Helper.Create_PLanet)
            {
                // Intert Method here 
                return Planets.Helper.None;
            }
            else
            {
                return Planets.Helper.None;
            }


            //switch (Helper)
            //{
            //    case Helper.None:
            //        //Execute this method;
            //        break;

            //    case Helper.Create_PLanet:
            //        //Execute this method;
            //        break;

            //    case Helper.Get_Planet:
            //        //Execute this method;
            //        break;

            //    case Helper.Order_By_Diameter:
            //        Console.WriteLine("Here is the Diameter's Ordered by size: ");
            //        Program.OrderByDiameter(); //<TODO> this need execute the its method. 
            //                                   // Not all code paths return a value. 
            //        break;

            //    case Helper.Order_By_Mass:
            //        Console.WriteLine("Here is the Mass's Ordered by size: ");
            //        Program.OrderByMass(); 
            //        break;

            //    case Helper.Order_By_Planet:
            //        Program.OrderByPlanet();
            //        break;
            //    default:

            //        break;

            //}

        }


    }
}


