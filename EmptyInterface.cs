﻿using System;
namespace Planets
{
    public interface IGetHelpable
    {

        void TypeOfPlanet();
        void GetHelp();


        // Error Handling for converting user inputs to planet values. 

        void IncorrectInput(); // returns --Incorrect input--
        void WhileIncorrectInput(); // Creates a while loop..,
        


    }
}
