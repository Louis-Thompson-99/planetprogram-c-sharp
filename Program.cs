﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planets
{
    public class Program  
    {
        static void Main(string[] args)
        {

            List <Planet> ListOfPlanets = new List<Planet>();


            /// Terrestrial 
            Planet Earth = new Planet("Earth", Composition.Terrestrial, 12742, 149.6, 0.873, "Our Home");
            Planet Mars = new Planet("Mars", Composition.Terrestrial, 6779, 227.9, 0.873, "Our Greatest hope of expanding to the final frontier. ");
            Planet Mercury = new Planet("Murcury", Composition.Terrestrial, 4879.4, 57.91, 0.055, "A large, Extremly hot planet with  suplphur dioxide in it's atmosphere");
            Planet Venus = new Planet("Venus", Composition.Terrestrial, 12104, 108.2, 0.815, "A Solid planet which orbits the sun closeley");
            /// Terrestrial 

            /// Jovian 
            Planet Jupiter = new Planet("Jupiter", Composition.Jovian, 139820317, 778.5, 8778.5, "This is the largest planet in the solar system. Comprised of ice and hydrogen. 11 moons, some of which may contian");
            Planet Saturn = new Planet("Saturn", Composition.Jovian, 116460, 1.434, 95.16, "Saturn's rings can be seen from earth With a Telescope. Some will even go as far as it bieng Visible to the naked eye ");
            Planet Neptune = new Planet("Neptune",Composition.Jovian, 49244, 4.495, 17.15, "Neptune, named after an acient Greek God of the sea.This is becuase of it's blue color ");
            Planet Uranus = new Planet("Uranus", Composition.Jovian, 50724, 2.871, 14.54, "Uranus, The Farthest away Planet in the solor system. That is if you exclude Pluto of course ");

            ListOfPlanets.Add(Earth);
            ListOfPlanets.Add(Mars);
            ListOfPlanets.Add(Mercury);
            ListOfPlanets.Add(Venus);

            ListOfPlanets.Add(Saturn);
            ListOfPlanets.Add(Neptune);
            ListOfPlanets.Add(Uranus);


            PlanetsToString(ListOfPlanets);
            Console.WriteLine(ListOfPlanets);
            //dump(Earth.ToString());
            Console.ReadLine();




        }

        public static void PlanetsToString(List<Planet> ListOfPlanets)
        {

            foreach (Planet planet in ListOfPlanets)
            {
                Console.WriteLine(planet.ToString());
                Console.WriteLine();
            }
        }

        public static  void OrderByMass(List<Planet> ListOfPlanets)
        {
                var OrderByMassPlanets = from p in ListOfPlanets
                                         orderby (p.Mass)
                                         select (p.Mass);
            //Rather than updating the list. A new one is made. 
            foreach (var planet in OrderByMassPlanets)
            {
                Console.WriteLine(planet); // There is no dump method in VS MAC OS edition. 
            }

        }

        public static void OrderByDiameter(List<Planet> ListOfPlanets)
        {
            var OrderByDiameterPlanets = from p in ListOfPlanets
                                         orderby (p.Diameter)
                                         select (p.Diameter);

            foreach (var planet in OrderByDiameterPlanets)
            {
                Console.WriteLine(planet);
            }

        }


        public static void OrderByPlanet(List<Planet> ListOfPlanets)
        {
            var OrderByPlanetType = ListOfPlanets.GroupBy(planet => planet.Composition)
                                                 .Select(
                                                    p => new
                                                    {
                                                        Composition = p.Key
                                                    });
            foreach (var planet in OrderByPlanetType)
            {
                Console.WriteLine(planet);
            }
        }

        public static void GetHelp()
        {

            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("Welcome to the Help Screen \nHere Select what you would need help on: ");
            string UserInput = Console.ReadLine();

            // Helper HelpChoice = (Helper)Enum.Parse(typeof(Helper), UserInput, true); // Converting to Enum


            try
            {
                Helper HelpChoice = (Helper)Enum.Parse(typeof(Helper), UserInput, true); // Converting to Enum
            }
            catch (Exception ex)
            {
                Console.WriteLine("This is not a Valid input. Please enter what is displayed ");
            }
            finally
            {
                Planet.Helper(HelpChoice, LisOfPlanets);

            }


        }

        public static void Making_A_Planet()
        {
            Console.WriteLine("------------------------------------------------------\n N E W  D I S C O V E R I E S\n------------------------------------------------------");

            Console.WriteLine("Please Enter The Name Of Planet: ");
            string PlanetName = Console.ReadLine();
            Console.WriteLine(PlanetName);

            Console.WriteLine("Is the Planet a Jovian(Gas), or a Teristrial?: ");
            string planetComposition = Console.ReadLine();
          
            Composition PlanetComposition = (Composition)Enum.Parse(typeof(Composition), planetComposition, true);

            while (PlanetComposition == false)                                                                                      //Do While? 
            {
                Console.WriteLine("You did not enter a valid Data entry. Please try again or enter 'Not Classified' if it is unknown: ");
                string UserInput = Console.ReadLine();

                if (UserInput == "Not Classified")
                {
                    PlanetComposition = Composition.NotClassified;
                }
                else
                {
                    Console.WriteLine("--Incorrect Input--\n This input is out of scope for Diameter.");
                }
            }


            ///// D I A M E T E R ///////
            do
            {
                bool Correct = false;
                Console.WriteLine("Enter the Diameter(Km) of the planet if known: ");
                string diameter = Console.ReadLine();
                //double Diameter = Convert.ToDouble(diameter);
                if (double.TryParse(diameter), out Diameter)
                {

                    PlanetDiameter = Diameter;
                    Correct = true;

                }
                else
                {
                    Console.WriteLine("--Incorrect Input-- \n This input is out of scope for Diameter.");
                    Correct = false;

                }
            } while (Correct == false);

            ///// D I A M E T E R ///////

        


        }


    }
}